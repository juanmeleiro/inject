#!/usr/bin/python3
import argparse
import json
import os
import sys # For general system interactions
import re
# import subprocess # For spawning subprocesses
# import readline # Makes input() behave as a shell, with history
# import signal # For handling signals
# import textwrap # For wraping text for printing to the terminal
# import termcolor # For coloring terminal output

# CONFIG_PATH = os.expanduser("~/.utility")
# CONFIG_FILE = CONFIG_PATH

START   = re.compile('-- @ (?P<identifier>.*?)[ \n]')
END     = re.compile('-- @')
REPLACE = re.compile('(?P<spaces> *)>>> (?P<identifier>.*?)[ \n]')
BEFORE  = '\\begin{{code*}}{{firstnumber={line},label={file}}}\n'
AFTER   = '\\end{{code*}}\n'
INDENT  = '    '

# class Line:
#     '''A line of code'''
#     def __init__(self, content, number):
#         self.content = content
#         self.number = number
# 
#     def __str__(self):
#         return "{} {}".format(self.number, self.content)
# 
#     def __repr__(self):
#         return "Line({}, {})".format(repr(self.content), repr(self.number))

class Block:
    def __init__(self, start):
        self.lines = []
        self.start = start

    def append(self, line):
        self.lines.append(line)

    def render(self, indent):
        return [indent + l for l in self.lines]

class Snippet:
    '''A collection of Lines from some file'''
    def __init__(self, filename, start):
        self.blocks = [Block(start)]
        self.filename = filename

    def append(self, line):
        self.blocks[-1].append(line)

    def jump(self, line_number):
        self.blocks.append(Block(line_number))

    def render(self, indent):
        out = []
        for b in self.blocks:
            data = {
                    "line": b.start,
                    "file": self.filename
                    }
            out.append(indent + BEFORE.format(**data))
            out.extend(b.render(indent + INDENT))
            out.append(indent + AFTER.format(**data))
        return out

def extract(code):
    '''Extract snippets from file `f` that are between `START` and `END`
    markers, storing them at the key given by the identifier matched in
    `START`.
    
    Disjoint snippets under the same identifier are stored together, separated
    by a newline.
    '''

    snippets = dict()
    copying = False
    line_number = 0
    identifier = None

    for line in code:
        if copying:
            # If we are copying code, we test for the END marker.
            match = END.match(line)
            # If we find it, we stop copying.
            if match:
                copying = False
            # Otherwise, just append the current line to the snippet and carry
            # on
            else:
                snippets[identifier].append(line)
        else:
            # If we are *not* copying code, we test for the START marker.
            match = START.match(line)
            # If the START marker matched a line, we either initialize it's list
            # of lines or add a \n, if it exists. Then, we start copying.
            if match:
                identifier = match.group('identifier')
                if not identifier in snippets:
                    snippets[identifier] = Snippet(code.name, line_number+1)
                else:
                    snippets[identifier].jump(line_number+1)
                copying = True

        line_number += 1 

    return snippets

def inject(documentation, snippets):
    '''Inject code snippets at d into documentation file f
    
    Code snippets are replaced into instances of the REPLACE marker that match
    their identifier.
    '''
    output = []
    for line in documentation:
        # For every line, we test for the REPLACE marker
        match = REPLACE.match(line)
        # If found, we `inject` the snippet
        if match:
            snippet = snippets[match.group('identifier')]
            spaces = match.group('spaces')
            output.extend(snippet.render(spaces)) # Inject the code
        else:
            # Otherwise, just copy the line over
            output.append(line)

    return output

parser = argparse.ArgumentParser(
    description='Inject code snippets from source into documentation.')

parser.add_argument('code',
    help = 'Path to code file.')

parser.add_argument('doc',
    help = 'Path to documentation file.')

parser.add_argument('out',
    help = 'Path to output file.',
    nargs = '?',
    default = False)

if __name__ == '__main__':
    args = parser.parse_args()

    # Extract snippets from code file
    try:
        code = open(args.code, 'r')
    except Exception as e:
        print('Problem reading {}: {}'.format(args.code, e))
        sys.exit(1)

    snippets = extract(code)
    code.close()
    
    # Substitute snippets into documentation file
    try:
        doc = open(args.doc, 'r')
    except Exception as e:
        print('Problem reading {}: {}'.format(args.doc, e))
        sys.exit(1)
    
    output = inject(doc, snippets)
    doc.close()

    # Write output file
    # If the user has specified a output file...
    if args.out:
        try:
            out = open(args.out, 'w')
        except:
            print('Problem opening {}'.format(args.out))
            sys.exit(1)
        out.writelines(output)
        out.close()
    # Otherwise, just pipe it to stdout
    else:
        for l in output:
            print(l, end='')

    sys.exit(0)
