# inject

## About

`inject` is a command line utility for... `inject`ing snippets of code into
marked-up documentation files. It generally looks like this: a source file will
have special comments that specify a snippet

```idris
-- @ test
test : Nat -> Nat
test n = 2*n
-- @
```

And a documentation file will have a marker

```latex
\begin{document}

    >>> test

\end{document}
```

The result `inject`ed document will look something like this:

```latex
\begin{document}

    \begin{code}
        test : Nat -> Nat
        test n = 2*n
    \end{code}

\end{document}
```

## Credits

The project icon was made by [Nikita Golubev][nikita] from
[Flaticon][flaticon]

[nikita]: https://www.flaticon.com/authors/nikita-golubev
[flaticon]: https://www.flaticon.com/
